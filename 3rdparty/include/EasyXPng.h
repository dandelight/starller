#pragma once
#include <graphics.h>
#include <math.h>
#define PI 3.14159

#ifndef EAXYX_PNG
#define EAXYX_PNG
// 在当前设备上绘制带透明通道的png图片
// img_x	绘制位置的x坐标
// img_y	绘制位置的y坐标
// pSrcImg	要绘制的IMAGE对象指针
extern void putimagePng(int img_x, int img_y, IMAGE *pSrcImg);

extern void putimagePartial(int dstX, int dstY, int dstWidth, int dstHeight,
                            IMAGE *pSrcImg, int srcX, int srcY);
extern void test();

#endif// EAXYX_PNG
