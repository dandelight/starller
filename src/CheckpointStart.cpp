//
// Created by 10402 on 2021/12/11.
//

#include "CheckpointStart.h"

CheckpointStart::CheckpointStart(int x, int y) : Collidable(x, y, width, height),
                                                 animation("F:\\workspace\\starller\\resource\\Items\\Checkpoints\\Start\\Start (Idle).png", "F:\\workspace\\starller\\resource\\Items\\Checkpoints\\Start\\Start (Moving) (64x64).png")
{

}

void CheckpointStart::init() {
    animation.triggered = 1;
}

void CheckpointStart::render() {
    animation.renderAt(x, y, width, height);
}
