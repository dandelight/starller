//
// Created by Min on 2021/12/11.
//

#include "Animation.h"
#include "easyx.h"
#include "EasyXPng.h"

Animation::Animation(LPCTSTR imagePath,int w, int h, int steps) {
    loadimage(&imageSeries, imagePath);
    playNthAnimation = 0;
    if(w==0) width=imageSeries.getwidth();
    else width = w;
    if(h==0) height=imageSeries.getheight();
    else height = h;
    if(steps == 0) mod = (imageSeries.getwidth()) / (imageSeries.getheight());
    else mod = steps;
}
void Animation::renderAt(int x, int y, int w, int h) {
    putimagePartial(x, y, w, h, &imageSeries, playNthAnimation*w, 0);
    playNthAnimation = (playNthAnimation + 1) % mod;
}
