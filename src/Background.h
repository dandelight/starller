//
// Created by Min on 2021/12/10.
//

#ifndef STARLLER_BACKGROUND_H
#define STARLLER_BACKGROUND_H

#include "easyx.h"
#include "Renderable.h"

class Background : public Renderable{
protected:
    IMAGE image;
    int offset = 0;
    int width_tiles, height_tiles, tile_length;
    Background(int w, int h, int len);

public:
    Background();
    Background(int w, int h, int len, LPCTSTR imagePath);
    void render() override;
    void init() override;
    void offsetLeft(int dx);
};


#endif//STARLLER_BACKGROUND_H
