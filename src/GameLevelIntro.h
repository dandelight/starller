//
// Created by Min on 2021/12/10.
//

#ifndef STARLLER_GAMELEVELINTRO_H
#define STARLLER_GAMELEVELINTRO_H
#include <list>
#include "GameLevel.h"
#include "Block.h"
#include "Trampoline.h"
#include "Fruit/Fruit.h"

class GameLevelIntro : public GameLevel{
private:
    std::list<Block> blocks;
    std::list<Fruit> fruits;
    Trampoline trampoline;
public:
    GameLevelIntro();
    void init() override;
    void run() override;
    void render() override;
    void updateWithInput() override;
    void updateWithoutInput() override;
    bool onLand() const override;
    void gameover();
};


#endif//STARLLER_GAMELEVELINTRO_H
