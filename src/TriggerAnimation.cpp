//
// Created by Min on 2021/12/11.
//

#include "TriggerAnimation.h"
#include "EasyXPng.h"
TriggerAnimation::TriggerAnimation(LPCTSTR staticImagePath, LPCTSTR imageSeriesPath):Animation(imageSeriesPath) {
    loadimage(&staticImage, staticImagePath);

    triggered = 0;
}
void TriggerAnimation::renderAt(int x, int y, int w, int h) {
    if(triggered == TRIGGERING) {
        putimagePartial(x, y, w, h, &imageSeries, playNthAnimation*w, 0);
        if(playNthAnimation == mod-1) triggered = TRIGGERED;
        playNthAnimation = (playNthAnimation + 1) % mod;
    }
    else {
        putimagePng(x, y, &staticImage);
    }
}

void TriggerAnimation::trigger() {
    triggered = TRIGGERING;
}
