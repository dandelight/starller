//
// Created by 10402 on 2021/12/11.
//

#include "CheckpointEnd.h"

#include "CheckpointEnd.h"
#include "TriggerAnimation.h"

CheckpointEnd::CheckpointEnd(int x, int y) : Collidable(x, y, width, height),
                                       animation("F:\\workspace\\starller\\resource\\Items\\Checkpoints\\End\\End (Idle).png", "F:\\workspace\\starller\\resource\\Items\\Checkpoints\\End\\End (Pressed) (64x64).png"){
}

void CheckpointEnd::init() {
    animation.triggered = 0;
}
void CheckpointEnd::render() {
    animation.renderAt(x, y, width, height);
}
