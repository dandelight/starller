//
// Created by Min on 2021/12/10.
//

#ifndef STARLLER_CHARACTER_H
#define STARLLER_CHARACTER_H
#include "Collidable.h"
#include "Renderable.h"
#include "easyx.h"
#include "PlayerState.h"
#include "Animation.h"

class Character : public Collidable, public Renderable {
protected:
    IMAGE imDoubleJump, imFall, imHit, imIdle, imJump, imRun, imWallJump;
    using pAnimation = Animation *;
    pAnimation anDoubleJump, anFall, anHit, anIdle, anJump, anRun, anWallJump;
    pAnimation anDoubleJumpLeft, anFallLeft, anHitLeft, anIdleLeft, anJumpLeft, anRunLeft, anWallJumpLeft;
    pAnimation animToPlay;
public:
    Character(int x, int y, int w, int h);
    virtual ~Character();
    void init() override = 0;
    void render() override;

    virtual void update() = 0;
    virtual void move(int dx, int dy) = 0;
    int playNthAnimation;
    PlayerState state;
};


#endif//STARLLER_CHARACTER_H
