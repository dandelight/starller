//
// Created by 10402 on 2021/12/11.
//

#ifndef STARLLER_FRUIT_H
#define STARLLER_FRUIT_H

#include "Collidable.h"
#include "easyx.h"
#include "Animation.h"


class Fruit : public Collidable, public Renderable {
public:
    const static int width = 32, height = 32;
    constexpr const static LPCTSTR imagePaths[] = {
         R"(F:\workspace\starller\resource\Items\Fruits\Apple.png)",
         R"(F:\workspace\starller\resource\Items\Fruits\Bananas.png)",
         R"(F:\workspace\starller\resource\Items\Fruits\Cherries.png)",
         R"(F:\workspace\starller\resource\Items\Fruits\Kiwi.png)",
         R"(F:\workspace\starller\resource\Items\Fruits\Melon.png)",
         R"(F:\workspace\starller\resource\Items\Fruits\Orange.png)",
         R"(F:\workspace\starller\resource\Items\Fruits\Pineapple.png)",
         R"(F:\workspace\starller\resource\Items\Fruits\Strawberry.png)"
    };
    Animation *animation;
    Fruit(int x, int y);
    ~Fruit();

public:
    void init() override;

    void render() override;
};


#endif //STARLLER_FRUIT_H
