//
// Created by 10402 on 2021/12/11.
//

#include "Fruit.h"
#include <random>
#include <ctime>
Fruit::Fruit(int x, int y) : Collidable(x, y, width, height){
    init();
}

void Fruit::init() {
    static std::mt19937 rng(time(nullptr));
    LPCTSTR imagePath = imagePaths[rng() % 8];
    animation = new Animation(imagePath);
}

Fruit::~Fruit() {
    delete animation;
}

void Fruit::render() {
    animation->renderAt(x, y, width, height);
}
