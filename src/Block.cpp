//
// Created by Min on 2021/12/10.
//

#include "Block.h"
#include "Collidable.h"
#include "Renderable.h"

Block::Block(int _x, int _y) : Collidable(_x, _y, Block::width, Block::height){
    loadimage(&image, R"(F:\workspace\clion_collide\resource\Terrain\grass.png)", width, height);
}
void Block::render() {
    putimage(x, y, &image);
}
void Block::init() {
}
