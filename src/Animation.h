//
// Created by Min on 2021/12/11.
//

#ifndef STARLLER_ANIMATION_H
#define STARLLER_ANIMATION_H

#include "easyx.h"
#include "Renderable.h"

class Animation {
protected:
    // int x, y, width, height;
    int width{}, height{};
    IMAGE imageSeries;
    int playNthAnimation{};
    int mod{};
public:
    Animation() = default;
    explicit Animation(LPCTSTR imagePath, int w=0, int h=0, int steps=0);
    virtual void renderAt(int x, int y, int w, int h);
};


#endif//STARLLER_ANIMATION_H
