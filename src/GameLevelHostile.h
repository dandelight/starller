//
// Created by Min on 2021/12/10.
//

#ifndef STARLLER_GameLevelHostile_H
#define STARLLER_GameLevelHostile_H
#include <list>
#include <vector>
#include "GameLevel.h"
#include "Block.h"
#include "Trampoline.h"
#include "Fruit/Fruit.h"
#include "Saw.h"

class GameLevelHostile : public GameLevel{
private:
    std::vector<Block> blocks;
    std::list<Fruit> fruits;
    std::vector<Saw> saws;
    Trampoline trampoline;
public:
    GameLevelHostile();
    void init() override;
    void run() override;
    void render() override;
    void updateWithInput() override;
    void updateWithoutInput() override;
    bool onLand() const override;

    void gameover();
};


#endif//STARLLER_GameLevelHostile_H
