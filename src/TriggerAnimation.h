//
// Created by Min on 2021/12/11.
//

#ifndef STARLLER_TRIGGERANIMATION_H
#define STARLLER_TRIGGERANIMATION_H

#include "Animation.h"

class TriggerAnimation : public Animation {
public:
    TriggerAnimation(LPCTSTR staticImagePath, LPCTSTR imageSeriesPath);
    const static int NOTTRIGGERED=0, TRIGGERING=1, TRIGGERED=2;
    int triggered;
    IMAGE staticImage;

    void trigger();
    void renderAt(int x, int y, int w, int h) override;
};


#endif//STARLLER_TRIGGERANIMATION_H
