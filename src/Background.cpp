//
// Created by Min on 2021/12/10.
//

#include "Background.h"
#include "easyx.h"
Background::Background():offset(0) {}

Background::Background(int w, int h, int len):width_tiles(w), height_tiles(h), tile_length(len) {}

Background::Background(int w, int h, int len, LPCTSTR imagePath):Background(w, h, len) {
    loadimage(&image, imagePath);
}

void Background::render() {
    for(int i=0; i<width_tiles; ++i) {
        for(int j=0; j<height_tiles; ++j) {
            putimage(i* tile_length - offset, j* tile_length, &image);
        }
    }
}

void Background::offsetLeft(int dx) {
    offset = int(double(offset) + double(dx)/2)%tile_length;
}
void Background::init() {
    offset = 0;
}
