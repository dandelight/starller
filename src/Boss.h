//
// Created by 10402 on 2021/12/12.
//

#ifndef STARLLER_BOSS_H
#define STARLLER_BOSS_H

#include "Collidable.h"
#include "Renderable.h"
#include "Animation.h"

class Boss : public Collidable, public Renderable {
private:
    Animation *animation;
public:
    const static int height = 52, width = 54;
    Boss(int x, int y);
    ~Boss();

    void move(int x, int y);
    void init() override;
    void render() override;
};


#endif //STARLLER_BOSS_H
