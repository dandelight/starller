//
// Created by Min on 2021/12/10.
//

#ifndef STARLLER_GAME_H
#define STARLLER_GAME_H
#include <list>
#include <memory>
#include "GameLevel.h"
#include "Renderable.h"

class Game final{
private:
    int height{}, width{};
    std::list<std::unique_ptr<GameLevel>> gameLevels;
public:
    Game(int w, int h);
    void init();
    void run();
    void render();
};


#endif//STARLLER_GAME_H
