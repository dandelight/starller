//
// Created by Min on 2021/12/10.
//

#include "Character.h"
#include "EasyXPng.h"

Character::Character(int x, int y, int w, int h) : Collidable(x, y, w, h) {}

Character::~Character() {}

void Character::render() {
  if (state.isIdle()) {
    if (speedX > 0)
      animToPlay = anIdle;
    else
      animToPlay = anIdleLeft;
  } else if (state.isJump()) {
    if (speedX > 0)
      animToPlay = anJump;
    else
      animToPlay = anJumpLeft;
  } else if (state.isDoubleJump()) {
    if (speedX > 0)
      animToPlay = anDoubleJump;
    else
      animToPlay = anDoubleJumpLeft;
  } else if (state.isRun()) {
    if (speedX > 0)
      animToPlay = anRun;
    else
      animToPlay = anRunLeft;
  } else {
    if (speedX > 0)
      animToPlay = anIdle;
    else
      animToPlay = anIdleLeft;
  }

  animToPlay->renderAt(x, y, width, height);
}
