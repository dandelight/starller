//
// Created by Min on 2021/12/11.
//

#ifndef STARLLER_TRAMPOLINE_H
#define STARLLER_TRAMPOLINE_H

#include "Renderable.h"
#include "Collidable.h"
#include "TriggerAnimation.h"

class Trampoline : public Renderable, public Collidable {
public:
    TriggerAnimation animation;
    const static int width = 28, height = 28;
    Trampoline(int x, int y);
    void init() override;
    void render() override;
};


#endif//STARLLER_TRAMPOLINE_H
