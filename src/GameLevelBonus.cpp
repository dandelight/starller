//
// Created by Min on 2021/12/10.
//

#include <fstream>
#include <conio.h>
#include "GameLevelBonus.h"
#include "CheckpointStart.h"
#include "CheckpointEnd.h"

auto showBox = [](Collidable&p) {
    rectangle(p.left(), p.top(), p.right(), p.bottom());
};

GameLevelBonus::GameLevelBonus(): GameLevel(15, 15, 63, R"(F:\workspace\starller\resource\Background\Purple.png)") {
}

void GameLevelBonus::init() {
    GameLevel::init();
    std::ifstream fin(R"(F:\workspace\starller\cfg\GameLevelBonusBlocks.cfg)");

    fruits.clear();
    score = 0;
    while(!fin.eof()) {
        int x, y; fin>>x>>y;
    }
    fin.close();

    std::ifstream fruits_config("F:\\workspace\\starller\\cfg\\GameLevelBonusFruits.cfg");
    while(!fruits_config.eof()) {
        int x, y; fruits_config>>x>>y;
        fruits.emplace_back(x, y);
    }

}
void GameLevelBonus::run() {
    GameLevel::run();
}
void GameLevelBonus::render() {
    BeginBatchDraw();
    background.render();
    for(auto &frt: fruits) {
        frt.render();
    }
    player.render();
//    GameLevel::render();
    static char buf[64];
    RECT r = {0, 0, 600, 800};
    sprintf(buf, "SCORE: %d", score);
    setbkmode(TRANSPARENT);
    //设置文本颜色
    settextcolor(BLUE);
    //设置文本样式
    settextstyle(30, 0, TEXT("consolas"));//字体的宽+字体的高+字体的风格
    drawtext(buf, &r, DT_WORDBREAK);
    ckptStart->render();
    if(fruits.empty()) ckptEnd->render();
    EndBatchDraw();
}
void GameLevelBonus::updateWithoutInput() {
    if(ckptEnd->animation.triggered == TriggerAnimation::TRIGGERED) {
        success();
    }

    if(player.nextStepUp() < 10) {
        player.speedY = 0;
        player.y = 10;
    }
    BeginBatchDraw();
    showBox(player);
    int vMid = (player.left()+player.right())/2;
    int hMid = (player.top() + player.bottom())/2;
    // checkpoint end
    if(fruits.empty() && vMid>ckptEnd->left() && vMid < ckptEnd->right() && hMid > ckptEnd->top() && hMid < ckptEnd->bottom()) {
        ckptEnd->animation.trigger();
    }

    EndBatchDraw();

    for(auto it=fruits.cbegin(), ed=fruits.cend(); it!=ed; ) {
        if (player.canEatFruit(*it)) {
            puts("Fruit Eaten!");
            score ++;
            it = fruits.erase(it);
            if(it == fruits.cend()) break;
        }
        else {
            ++it;
        }
    }

    GameLevel::updateWithoutInput();
}
bool GameLevelBonus::onLand() const {
    return false;
}


void GameLevelBonus::updateWithInput() {
    if(onLand()) {
        player.speedY = 0;
        player.state.switchState(PlayerState::IDLE);
    }

    if (_kbhit()) {
        printf("KBHIT\n");
        if (GetAsyncKeyState(VK_RIGHT) || GetAsyncKeyState('L')) {
            moveRight();
        }
        else if (GetAsyncKeyState(VK_LEFT) || GetAsyncKeyState('H')) {
            moveLeft();
        }
        if (GetAsyncKeyState(VK_UP) || GetAsyncKeyState('J') || GetAsyncKeyState(' ')) {
            jump();
        }
        if(GetAsyncKeyState('F')) {
//            printf("[DEBUG] player at: t:%d r:%d b:%d l:%d x:%d y:%d\n", player.top(), player.right(), player.bottom(), player.left(),
//                   (player.left()+player.right())/2, (player.top()+player.bottom())/2
//            );
            printf("%d %d\n", player.left(), player.top());
        }
    }
}

void GameLevelBonus::jump() {
//    if(!player.state.isJump()) {
    player.state.switchState(PlayerState::JUMP);
    player.speedY -= 2;
//    }
}

void GameLevelBonus::gameover() {
    setbkmode(TRANSPARENT);
    RECT r = {0, 0, width, height};
    drawtext("GAME OVER", &r, DT_CENTER | DT_VCENTER | DT_SINGLELINE);
    system("PAUSE");
    init();
}
