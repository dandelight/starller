//
// Created by Min on 2021/12/10.
//

#ifndef STARLLER_COLLIDABLE_H
#define STARLLER_COLLIDABLE_H


class Collidable {
public:
    Collidable(int x, int y, int w, int h):x(x), y(y), width(w), height(h), speedX(0), speedY(0){}
    int x, y, width, height;
    double speedX, speedY;
    inline int top() const {return y;}
    inline int bottom() const {return y+height;}
    inline int left() const {return x;}
    inline int right() const {return x+width;}
    inline int nextStepRight() const {return x+width+speedX;}
    inline int nextStepLeft() const {return x+speedX;}
    inline int nextStepUp() const {return y+speedY;}
    inline int nextStepDown() const {return y+height+speedY;}
};


#endif//STARLLER_COLLIDABLE_H
