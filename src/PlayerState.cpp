//
// Created by Min on 2021/12/10.
//

#include <cstdio>
#include "PlayerState.h"
bool PlayerState::isJump() const {
    return state & JUMP;
}
bool PlayerState::isDoubleJump() const {
    return state & DOUBLEJUMP;
}
bool PlayerState::isFall() const {
    return state & FALL;
}
bool PlayerState::isHit() const {
    return state & HIT;
}
bool PlayerState::isIdle() const {
    return state & IDLE;
}
bool PlayerState::isRun() const {
    return state & RUN;
}
bool PlayerState::isWallJump() const {
    return state & WALLJUMP;
}
bool PlayerState::canJump() const {
    if(isFall()) {
        printf("You are falling\n");
    }
    return !(isJump() || isDoubleJump() || isFall());
}

bool PlayerState::canDoubleJump() const {
    return (isJump() && !isDoubleJump()) || isFall();
}

void PlayerState::switchState(int newState) {
    state = newState;
}
