//
// Created by Min on 2021/12/10.
//

#ifndef STARLLER_GameLevelBoss_H
#define STARLLER_GameLevelBoss_H
#include <list>
#include <vector>
#include "GameLevel.h"
#include "Block.h"
#include "Trampoline.h"
#include "Fruit/Fruit.h"
#include "Saw.h"
#include "Boss.h"

class GameLevelBoss : public GameLevel{
private:
    std::vector<Block> blocks;
    std::list<Fruit> fruits;
    std::vector<Saw> saws;
    Boss boss;
    Trampoline trampoline;
public:
    GameLevelBoss();
    void init() override;
    void run() override;
    void jump() override;
    void render() override;
    void updateWithInput() override;
    void updateWithoutInput() override;
    bool onLand() const override;
    void gameover();
};


#endif//STARLLER_GameLevelBoss_H
