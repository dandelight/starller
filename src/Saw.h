//
// Created by 10402 on 2021/12/11.
//

#ifndef STARLLER_SAW_H
#define STARLLER_SAW_H

#include "Renderable.h"
#include "Collidable.h"
#include "TriggerAnimation.h"

class Saw : public Renderable, public Collidable {
public:
    Animation animation;
    const static int width = 38, height = 38;
    Saw(int x, int y);
    void init() override;
    void render() override;
};


#endif //STARLLER_SAW_H
