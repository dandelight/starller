//
// Created by 10402 on 2021/12/11.
//

#ifndef STARLLER_CHECKPOINTEND_H
#define STARLLER_CHECKPOINTEND_H

#include "Renderable.h"
#include "Collidable.h"
#include "TriggerAnimation.h"

class CheckpointEnd : public Renderable, public Collidable {
public:
    TriggerAnimation animation;
    const static int width = 64, height = 64;
    CheckpointEnd(int x, int y);
    void init() override;
    void render() override;
};

#endif //STARLLER_CHECKPOINTEND_H
