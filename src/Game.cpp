//
// Created by Min on 2021/12/10.
//

#include <memory>
#include "Game.h"
#include "GameLevel.h"
#include "GameLevelIntro.h"
#include "GameLevelHostile.h"
#include "GameLevelBoss.h"
#include "GameLevelBonus.h"
#include <easyx.h>

Game::Game(int w, int h):width(w), height(h) {

}
void Game::init() {
    initgraph(width, height, EW_SHOWCONSOLE);
    //gameLevels.push_back(std::unique_ptr<GameLevel>(new GameLevelBonus));
    gameLevels.push_back(std::unique_ptr<GameLevel>(new GameLevelIntro));
    gameLevels.push_back(std::unique_ptr<GameLevel>(new GameLevelHostile));
    gameLevels.push_back(std::unique_ptr<GameLevel>(new GameLevelBoss));
//    gameLevels.push_back(std::make_unique<GameLevel>( 15, 15, 63, R"(F:\workspace\starller\resource\Background\Green.png)"));
//    gameLevels.push_back(std::make_unique<GameLevel>(15, 15, 63, R"(F:\workspace\starller\resource\Background\Purple.png)"));
//    gameLevels.push_back(std::make_unique<GameLevel>(15, 15, 63, R"(F:\workspace\starller\resource\Background\Yellow.png)"));
//    gameLevels.push_back(std::make_unique<GameLevel>(15, 15, 63, R"(F:\workspace\starller\resource\Background\Gray.png)"));
}
void Game::run() {
    for(const auto &gameLevel : gameLevels) {
        gameLevel->init();
        while(true) {
            gameLevel->render();
            gameLevel->updateWithInput();
            gameLevel->updateWithoutInput();
            if(gameLevel->isFinished()) break;
            Sleep(15);
        }
    }
}
