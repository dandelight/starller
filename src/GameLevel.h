//
// Created by Min on 2021/12/10.
//

#ifndef STARLLER_GAMELEVEL_H
#define STARLLER_GAMELEVEL_H

#include "Background.h"
#include "Renderable.h"
#include "Player.h"
#include "CheckpointStart.h"
#include "CheckpointEnd.h"

class GameLevel : public Renderable {
protected:
    int height, width;
    Background background;
    Player player;
    CheckpointStart *ckptStart;
    CheckpointEnd *ckptEnd;
    bool finished = false;

    double gravity = 0.5;
    time_t lastJump = 0;
    int score = 0;

    virtual void moveLeft();
    virtual void moveRight();
    virtual void jump();
public:
    GameLevel();
    GameLevel(int w, int h, int len, LPCTSTR bgImagePath);
    void init() override;
    virtual void run();
    void render() override;
    virtual void updateWithInput();
    virtual void updateWithoutInput();
    virtual void success();
    inline virtual bool isFinished() const {return finished;};

    virtual bool onLand() const;
    void gameover();
};


#endif//STARLLER_GAMELEVEL_H
