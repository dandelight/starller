//
// Created by 10402 on 2021/12/12.
//

#ifndef STARLLER_GAMELEVELBONUS_H
#define STARLLER_GAMELEVELBONUS_H

#include "GameLevel.h"
#include <list>
#include "Fruit/Fruit.h"

class GameLevelBonus : public GameLevel {
    std::list<Fruit> fruits;
public:
    GameLevelBonus();
    void init() override;
    void run() override;
    void jump() override;
    void render() override;
    void updateWithInput() override;
    void updateWithoutInput() override;
    bool onLand() const override;
    void gameover();
};


#endif //STARLLER_GAMELEVELBONUS_H
