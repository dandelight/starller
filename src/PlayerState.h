//
// Created by Min on 2021/12/10.
//

#ifndef STARLLER_PLAYERSTATE_H
#define STARLLER_PLAYERSTATE_H


struct PlayerState {
    static const int JUMP=0x1, DOUBLEJUMP=0x2, FALL=0x4, HIT=0x8, IDLE=0x10, RUN=0x20, WALLJUMP=0x40;
    int state;
    bool isJump() const, isDoubleJump() const, isFall() const, isHit() const, isIdle() const, isRun() const , isWallJump() const;
    bool canJump() const, canDoubleJump() const;
    void switchState(int newState);
};


#endif//STARLLER_PLAYERSTATE_H
