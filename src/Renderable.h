//
// Created by Min on 2021/12/10.
//

#ifndef STARLLER_RENDERABLE_H
#define STARLLER_RENDERABLE_H


class Renderable {
public:
    virtual void init() = 0;
    virtual void render() = 0;
};


#endif//STARLLER_RENDERABLE_H
