//
// Created by Min on 2021/12/10.
//
#include <conio.h>
#include <cstdio>
#include <ctime>
#include "GameLevel.h"
#include "easyx.h"

GameLevel::GameLevel() : player(0,0,32,32 ){}

//GameLevel::GameLevel(const Background &background) : background(background) {}

GameLevel::GameLevel(int w, int h, int len, LPCTSTR bgImagePath):background(w, h, len, bgImagePath),
width(w*len), height(h*len), player(32, 400, 32, 32) {
    ckptStart = new CheckpointStart(20, 820);
    ckptEnd = new CheckpointEnd(240, 128);
}
void GameLevel::init() {
    background.init();
    player.init();
    ckptStart->init();
    ckptEnd->init();
}

void GameLevel::run() {
    init();
    while(true) {
        render();
        updateWithInput();
        updateWithoutInput();
        if(finished) break;
        Sleep(15);
    }
}

void GameLevel::render() {
    background.render();
    player.render();
    ckptStart->render();
    ckptEnd->render();

}

void GameLevel::moveLeft() {
    if(!player.state.isJump() && !player.state.isDoubleJump()) {
        memset(&player.state, 0, sizeof player.state);
        player.state.switchState(PlayerState::RUN);
    }
    player.speedX -= 4;
}

void GameLevel::moveRight() {
    if(!player.state.isJump() && !player.state.isDoubleJump()) {
        memset(&player.state, 0, sizeof player.state);
        player.state.switchState(PlayerState::RUN);
    }
    player.speedX += 4;
}
void GameLevel::jump() {
    const time_t thisJump = GetTickCount();
    if(thisJump - lastJump < 300) {
        return;
    }
    else {
        lastJump = thisJump;
    }
    if(player.state.isFall()) {
        printf("Falling\n");
    }
    if(player.state.canJump()) {
        player.state.switchState(PlayerState::JUMP);
        player.speedY -= 10;
    }
    else if(player.state.canDoubleJump()) {
        player.state.switchState(PlayerState::DOUBLEJUMP);
        player.speedY -= 4;
    }

}

void GameLevel::updateWithInput() {
//    if (_kbhit()) {
//        if (GetAsyncKeyState(VK_RIGHT) || GetAsyncKeyState('L')) {
//            moveRight();
//        }
//        else if (GetAsyncKeyState(VK_LEFT) || GetAsyncKeyState('H')) {
//            moveLeft();
//        }
//        if (GetAsyncKeyState(VK_UP) || GetAsyncKeyState('J') || GetAsyncKeyState(' ')) {
//            jump();
//        }
//        if(GetAsyncKeyState('F')) {
////            printf("[DEBUG] player at: t:%d r:%d b:%d l:%d x:%d y:%d\n", player.top(), player.right(), player.bottom(), player.left(),
////                   (player.left()+player.right())/2, (player.top()+player.bottom())/2
////            );
//            printf("%d %d\n", player.left(), player.top());
//        }
//    }
}

void GameLevel::updateWithoutInput() {
    // 用环境更新用户
    if(player.speedY > 0 && !(player.state.isJump() || player.state.isDoubleJump())) {
        player.state.switchState(PlayerState::FALL);
//        printf("Switched to FALL\n");
    }
    // Gravity
    player.speedY += gravity;
    player.speedX /= 2;
    player.move(player.speedX, player.speedY);

    if(player.bottom() + 1>= height) {
        gameover();
    }
}

void GameLevel::gameover() {

}

bool GameLevel::onLand() const {
    return false;
}

void GameLevel::success() {
    finished = true;
}
