//
// Created by Min on 2021/12/10.
//

#ifndef STARLLER_BLOCK_H
#define STARLLER_BLOCK_H


#include "Renderable.h"
#include "easyx.h"
#include "Collidable.h"
class Block : public Renderable, public Collidable {
private:
    IMAGE image;
public:
    static const int height = 63;
    static const int width = 63;

    Block(int _x = 0, int _y = 0);
    void render();
    void init() override;
};


#endif//STARLLER_BLOCK_H
