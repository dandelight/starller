//
// Created by Min on 2021/12/10.
//

#ifndef STARLLER_PLAYER_H
#define STARLLER_PLAYER_H

#include "Character.h"
#include "Fruit/Fruit.h"

class Player : public Character{
public:
    Player(int x, int y, int w, int h);
    void update() override;
    void move(int dx, int dy) override;
    void init() override;

    bool canEatFruit( const Fruit & f);
//    void render() override;
};


#endif//STARLLER_PLAYER_H
