//
// Created by Min on 2021/12/11.
//

#include "Trampoline.h"
#include "TriggerAnimation.h"

Trampoline::Trampoline(int x, int y) : Collidable(x, y, width, height),
animation("F:\\workspace\\starller\\resource\\Traps\\Trampoline\\Idle.png", "F:\\workspace\\starller\\resource\\Traps\\Trampoline\\Jump (28x28).png"){
}

void Trampoline::init() {

}
void Trampoline::render() {
    animation.renderAt(x, y, width, height);
}
