//
// Created by 10402 on 2021/12/12.
//

#include "Boss.h"
#include "Collidable.h"

Boss::Boss(int x, int y) : Collidable(x, y, width, height) {
    animation = new Animation("F:\\workspace\\starller\\resource\\Traps\\Spike Head\\Blink (54x52).png");
}

Boss::~Boss() {
    delete animation;
}

void Boss::init() {

}


void Boss::render() {
    animation->renderAt(x, y, width, height);
}

void Boss::move(int dx, int dy) {
    x += dx;
    y += dy;
}
