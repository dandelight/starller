//
// Created by 10402 on 2021/12/11.
//

#ifndef STARLLER_CHECKPOINTSTART_H
#define STARLLER_CHECKPOINTSTART_H

#include "Renderable.h"
#include "Collidable.h"
#include "TriggerAnimation.h"

class CheckpointStart : public Renderable, public Collidable {
public:
    TriggerAnimation animation;
    const static int width = 64, height = 64;
    CheckpointStart(int x, int y);
    void init() override;
    void render() override;
};


#endif //STARLLER_CHECKPOINTSTART_H
