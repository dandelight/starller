//
// Created by Min on 2021/12/10.
//

#include <cstring>
#include <cstdio>

#include "easyx.h"
#include "EasyXPng.h"
#include "Player.h"
#include "Collidable.h"
#include <mmsystem.h>
#pragma comment(lib, "winmm.lib")

Player::Player(int x, int y, int w, int h) : Character(x, y, w, h) {
    loadimage(&imDoubleJump,
              "F:\\workspace\\starller\\resource\\Main Characters\\Mask "
              "Dude\\Double Jump (32x32).png");
    loadimage(&imFall,
              "F:\\workspace\\starller\\resource\\Main Characters\\Mask "
              "Dude\\Fall (32x32).png");
    loadimage(&imHit,
              "F:\\workspace\\starller\\resource\\Main Characters\\Mask "
              "Dude\\Hit (32x32).png");
    loadimage(&imIdle,
              "F:\\workspace\\starller\\resource\\Main Characters\\Mask "
              "Dude\\Idle (32x32).png");
    loadimage(&imJump,
              "F:\\workspace\\starller\\resource\\Main Characters\\Mask "
              "Dude\\Jump.png");
    loadimage(&imRun,
              "F:\\workspace\\starller\\resource\\Main Characters\\Mask "
              "Dude\\Run (32x32).png");
    loadimage(&imWallJump,
              "F:\\workspace\\starller\\resource\\Main Characters\\Mask "
              "Dude\\Wall Jump (32x32).png");


    anDoubleJump = new Animation(
              "F:\\workspace\\starller\\resource\\Main Characters\\Mask "
              "Dude\\Double Jump (32x32).png");
    anFall = new Animation(
              "F:\\workspace\\starller\\resource\\Main Characters\\Mask "
              "Dude\\Fall (32x32).png");
    anHit  = new Animation(
              "F:\\workspace\\starller\\resource\\Main Characters\\Mask "
              "Dude\\Hit (32x32).png");
    anIdle = new Animation(
              "F:\\workspace\\starller\\resource\\Main Characters\\Mask "
              "Dude\\Idle (32x32).png");
    anJump = new Animation(
              "F:\\workspace\\starller\\resource\\Main Characters\\Mask "
              "Dude\\Jump.png");
    anRun = new Animation(
              "F:\\workspace\\starller\\resource\\Main Characters\\Mask "
              "Dude\\Run (32x32).png");
    anWallJump = new Animation(
              "F:\\workspace\\starller\\resource\\Main Characters\\Mask "
              "Dude\\Wall Jump (32x32).png");

    anDoubleJumpLeft = new Animation(
            "F:\\workspace\\starller\\resource\\Main Characters\\Mask "
            "Dude\\Double Jump (32x32)leftward.png");
    anFallLeft = new Animation(
            "F:\\workspace\\starller\\resource\\Main Characters\\Mask "
            "Dude\\Fall (32x32)leftward.png");
    anHitLeft  = new Animation(
            "F:\\workspace\\starller\\resource\\Main Characters\\Mask "
            "Dude\\Hit (32x32)leftward.png");
    anIdleLeft = new Animation(
            "F:\\workspace\\starller\\resource\\Main Characters\\Mask "
            "Dude\\Idle (32x32)leftward.png");
    anJumpLeft = new Animation(
            "F:\\workspace\\starller\\resource\\Main Characters\\Mask "
            "Dude\\Jumpleftward.png");
    anRunLeft = new Animation(
            "F:\\workspace\\starller\\resource\\Main Characters\\Mask "
            "Dude\\Run (32x32)leftward.png");
    anWallJumpLeft = new Animation(
            "F:\\workspace\\starller\\resource\\Main Characters\\Mask "
            "Dude\\Wall Jump (32x32)leftward.png");
}
void Player::update() {
}
void Player::move(int dx, int dy) {
    x += dx, y += dy;
}
void Player::init() {
    state.switchState(PlayerState::IDLE);
    speedX = speedY = 0;
    x = 32;
    y = 800;
    state.switchState(PlayerState::FALL);
}

bool Player::canEatFruit(const Fruit &f) {
    int hMid = (f.top()+f.bottom())/2;
    int vMid = (f.left() + f.right())/2;
    bool ans = (left() < vMid && right() > vMid && top()<hMid && bottom()>hMid);
    if(ans) {
        mciSendString("close eatcoin", nullptr, 0, nullptr);
        mciSendString(TEXT("open F:\\workspace\\starller\\resource\\Music\\eatcoin.wav alias eatcoin"), NULL, 0, NULL);
        mciSendString("play eatcoin", nullptr, 0, nullptr);
//        PlaySound(R"(F:\workspace\starller\resource\Music\eatcoin.wav)", nullptr, SND_FILENAME | SND_ASYNC);
    }
    return ans;
}
