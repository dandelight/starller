function revertImage(inPath, outPath, stride)
    [im, map, a] = imread(inPath);
    [h, w, c] = size(im)
    newim = im;
    newa = a;
    if (nargin < 3)
        stride = h;
    end
    
    mod = w / stride
    
    for i = 0:mod-1
       for j = 1:stride
          for k = 1:h
              newim(k, i*stride + j, :) = im(k, (i+1)*stride+1-j, :);
              newa(k, i*stride+j) = a(k, (i+1)*stride+1-j);
          end
       end
    end
    imwrite(newim, outPath, "Alpha", newa)
end